INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)

ADD_EXECUTABLE(logger Logger.cpp)

ADD_EXECUTABLE(bot Bot.cpp)
				
ADD_EXECUTABLE(servidor servidor.cpp MundoServidor.cpp ${COMMON_SRCS})
ADD_EXECUTABLE(cliente cliente.cpp MundoCliente.cpp ${COMMON_SRCS})

TARGET_LINK_LIBRARIES(servidor glut GL GLU pthread)
TARGET_LINK_LIBRARIES(cliente glut GL GLU)

