EJERCICIO INDIVIDUAL

En este apartado he añadido una funcionalidad al programa. Ahora, la esfera disminuye su radio hasta un valor mínimo. Cuando la esfera toque alguna de las dos paredes laterales, el radio vuelve a su valor por defecto.
