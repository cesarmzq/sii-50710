// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{	
	radio_min=0.25f;
	radio=0.5f;
	radio_max=1.5f;
	velocidad.x=3;
	velocidad.y=3;
	pulso=1.5f;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t;
	if ((radio>radio_max)||(radio<radio_min))
		pulso=-pulso;
	radio+=pulso*t;
	
	
}
