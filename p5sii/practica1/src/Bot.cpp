#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemoriaCompartida.h"
#include "Esfera.h"

int main()
{
	int file;
	DatosMemoriaCompartida* pMemComp;
	char* org;

	file=open("/tmp/datosComp.txt",O_RDWR);

	org=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);
	
	pMemComp=(DatosMemoriaCompartida*)org;


	while(1)
	{
		float posicion_raqueta;

		posicion_raqueta=(pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2;
		if(posicion_raqueta<pMemComp->esfera.centro.y)		
			pMemComp->accion=1;				//La raqueta sube
		else if(posicion_raqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;				//La raqueta baja
		else
			pMemComp->accion=0;				//La raqueta esta a la altura de la bola
		usleep(25000);
	}

	munmap(org,sizeof(*(pMemComp)));
	
	return 0;
}
