#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define TAM 200

int main() 
{

	//Borrar la tubería en la consola: rm /tmp/TuberiaLogger
	int e=mkfifo("/tmp/TuberiaLogger",0777);

	if(e==-1)//si no se ha creado correctamente
		{
		perror("Fallo al crear la tuberia");
		exit(1);
		}	

	mkfifo("/tmp/TuberiaLogger",0777);

 	int pipe=open("/tmp/TuberiaLogger",O_RDONLY);

	while(1) {
		char cad[TAM];
		int aux=read(pipe, cad, sizeof(cad));//lectura de la tubería
	
	if(aux==-1){
			perror("Fallo al leer la tuberia");
			exit(1);}


		if(aux==0)
			exit(0);
		printf("%s\n", cad);

	read (pipe, cad, sizeof(cad));		

 		printf("%s\n", cad);
	}

	int closepipe=close(pipe);//cerramos la tuberia
	if (closepipe=-1)
	{
		perror("Fallo al cerrar tubería\n");
		exit(0);
	}
	unlink("/tmp/TuberiaLogger");

	return 0;
}

